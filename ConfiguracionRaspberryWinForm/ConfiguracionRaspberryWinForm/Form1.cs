﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfiguracionRaspberryWinForm
{
    
    public partial class Form1 : Form
    {
        public static HttpClient client = new HttpClient();
        bool existeNombreDispositivo = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private static string[] SeleccionarInformacionWIFI()
        {
            ProcessStartInfo info = new ProcessStartInfo("netsh", "wlan show interfaces");
            info.WorkingDirectory = "";
            info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            info.CreateNoWindow = true;
            info.RedirectStandardOutput = true;
            info.UseShellExecute = false;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = info;
            proc.Start();
            var ex = proc.StandardOutput.ReadToEnd();
            string[] lista = ex.Split(Convert.ToChar("\n"));
            return lista;

        }

        private static string ObtenerSSID(string[] lista)
        {
            bool Existe = false;
            string SSID = "SinWIFI";
            for (int i = 0; i < lista.Length && !Existe; i++)
            {
                var item = lista[i];
                if ((item.Length > 4) && (item.Trim().Substring(0, 4).ToUpper() == "SSID"))
                {
                    Existe = true;
                    string[] aux = item.Split(Convert.ToChar(":"));
                    SSID = aux[1].Substring(0, aux[1].Length - 1);
                }
            }
            if (Existe)
                return SSID;
            return "";
        }

        private async Task<bool> VerificarNombreRaspBerry(string NombreHotel)
        {
            string path = "https://hoteldeabejas-619ca.firebaseio.com/hoteles/names.json";
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                var lstHoteles = JsonConvert.DeserializeObject<Dictionary<string, Hotel>>(json);
                foreach (var item in lstHoteles)
                {
                    if (item.Key.ToUpper() == NombreHotel.ToUpper())
                    {
                        existeNombreDispositivo = true;
                        return existeNombreDispositivo;
                    }
                }
            }
            return existeNombreDispositivo;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] lista = SeleccionarInformacionWIFI();
            txtSSID.Text = ObtenerSSID(lista).Trim();
            txtCanal.Text = SeleccionarCanal(lista);
        }

        private string SeleccionarCanal(string[] lista)
        {
            bool Existe = false;
            string Canal = "1";
            for (int i = 0; i < lista.Length && !Existe; i++)
            {
                var item = lista[i];
                if ((item.Length > 4) && (item.Trim().Substring(0, 5).ToUpper() == "CANAL"))
                {
                    Existe = true;
                    string[] aux = item.Split(Convert.ToChar(":"));
                    Canal = aux[1].Substring(0, aux[1].Length - 1);
                }
            }
            if (Existe)
                return Canal;
            return "1";
        }

        private void GuardarArchivos(string ubicacion, string ArchivoConfiguracion, string ArchivoHostName)
        {
            folderBrowserDialog1.ShowDialog();
            string URL = folderBrowserDialog1.SelectedPath;
            string FULLURL = Path.Combine(URL, ubicacion);
            try
            {

                if (File.Exists(FULLURL))
                {
                    File.Delete(FULLURL);
                }
                using (FileStream archivo = File.Create(FULLURL))
                {
                    Byte[] informacion = new UTF8Encoding(true).GetBytes(ArchivoConfiguracion);
                    archivo.Write(informacion, 0, informacion.Length);
                }
                //GuardarHostname
                string FULLURLHostName = Path.Combine(URL, "hostname");
                if (File.Exists(FULLURLHostName))
                {
                    File.Delete(FULLURLHostName);
                }
                using (FileStream archivo = File.Create(FULLURLHostName))
                {
                    Byte[] informacion = new UTF8Encoding(true).GetBytes(ArchivoHostName);
                    archivo.Write(informacion, 0, informacion.Length);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo Crear el Archivo");
            }


        }


        private void button1_Click(object sender, EventArgs e)
        {
            existeNombreDispositivo = false;
            VerificarNombreRaspBerry(txtNombreDispositivo.Text);
            if (!existeNombreDispositivo)
            {

            string ubicacion = "wpa_supplicant.conf";
            string ArchivoConfiguracion = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev \n\r";
            ArchivoConfiguracion += "update_config = 1  \n\r";
                ArchivoConfiguracion += "country=GB  \n\r";
                ArchivoConfiguracion += "network ={ \n\r";
            ArchivoConfiguracion += "                ssid = \"" + txtSSID.Text.Trim() + "\" \n\r";
            if (ddlEscriptacion.Text!="NONE")
                {
                    ArchivoConfiguracion += " psk = \"" + txtPassword.Text + "\" \n\r";
                    ArchivoConfiguracion += " key_mgmt="+ ddlEscriptacion.Text + "  \n\r";
                }
            else
                    ArchivoConfiguracion += " key_mgmt=NONE  \n\r";
            ArchivoConfiguracion += "}\n\r";
            string ArchivoHostname = txtNombreDispositivo.Text.Trim();
            GuardarArchivos(ubicacion, ArchivoConfiguracion, ArchivoHostname);
            MessageBox.Show("La Configuración se creo Correctamente");
            }

            else
            {
                MessageBox.Show("El Nombre del dispositivo ya existe en la Red, elija otro!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Vista a = new Vista();
            a.ShowDialog();
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            if (txtNombreDispositivo.Text.Length>=5)
            {

            existeNombreDispositivo = false;
            bool Disponibilidad = await VerificarNombreRaspBerry(txtNombreDispositivo.Text);
            if (Disponibilidad)
                MessageBox.Show("El Nombre del dispositivo ya existe en la Red, elija otro!");
            else
                MessageBox.Show("El Nombre esta Disponible!, tu Hotel se llamará: "+ txtNombreDispositivo.Text);
            }
            else
                MessageBox.Show("El Nombre debe contar con mas de 5 Caracteres");
        }
        private string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.- ]+", "", RegexOptions.Compiled);
        }

        private void txtNombreDispositivo_KeyDown(object sender, KeyEventArgs e)
        {
            txtNombreDispositivo.Text = RemoveSpecialCharacters(txtNombreDispositivo.Text);
        }

        private void txtNombreDispositivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //&& !char.IsWhiteSpace(e.KeyChar) 
            if (!char.IsLetter(e.KeyChar) && !char.IsNumber(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (ddlEscriptacion.Text == "NONE")
            {
                txtPassword.Enabled = false;
                txtPassword.Text = "";
            }
            else
                txtPassword.Enabled = true;
        }
    }
}
