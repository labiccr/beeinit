## Proyecto Hotel inteligente de Abejas
#### <http://www.hoteldeabejas.org>
#### Laboratorio LABIC / Costa rica 2019

#### Qué es un hotel para abejas?
<br>
Es una estructura de construcción artificial que sirve como refugio para abejas y avispas nativas de
anidación solitaria pertenecientes a diversos géneros. Se puede ubicar dentro de áreas urbanas y
rurales en las cuales se han reducido los sitios de anidación habituales debido principalmente a la
deforestación, al cambio de uso de suelo y al uso de agroquímicos.
<br>
#### Qué lo hace inteligente?
<br>
El Hotel Inteligente para Abejas se denomina así porque aparte de proporcionar un refugio
adecuado cuenta con una serie de sensores y aparatos electrónicos que generan constantemente
información relacionada con las condiciones ambientales de la zona, como temperatura, humedad
y presión, Radiación Ultravioleta y contaminantes del aire, a la vez que monitorea la ocupación del
hotel mediante una cámara fotográfica activada por un sensor de movimiento.
<br>


#### Instalacion de archivos necesarios para puesta en marcha.